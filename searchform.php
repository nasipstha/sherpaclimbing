<!-- <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    <div><label class="screen-reader-text" for="s">Search for:</label>
        <input type="text" value="" name="s" id="s" />
        <input type="submit" id="searchsubmit"  />
    </div>
</form> -->


<div class="navbar-search_desktop visible-md visible-lg" style="float:right;">
	<div class="btn-srch"><i class="fa fa-search"></i></div>
	<div class=" hdr-srch-overlay">
    <button class="srch-close btn btn-search" style="margin-top:0px"><i class="fa fa-times"></i></button>        
        <form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
            <div>
                <input type="text" value="" name="s" id="s" placeholder="Explore us...">
                <button type="submit" id="searchsubmit" value=" " class="btn btn-search" style="margin-left:5px;"><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>
</div>