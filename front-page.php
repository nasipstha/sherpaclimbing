<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Treaking_Hub_Nepal
 */

get_header(); ?>

        <!-- <div><h1>slider below</h1></div> -->
            <div class="slider-area body-wrapper">
                <?php 	
                $post_id = "";
                query_posts('cat=5'); ?>
                <div class="slider">
                <div id="bg-slider" class="owl-carousel owl-theme">
                    <?php while (have_posts()) : the_post(); ?>
                    <div class="item">
                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>" alt="<?php echo the_title();?>">
                    <div class="slider-content">
                        <div class="row">
                            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-sm-12">
                                <h2  id="slidertitleformserver"><?php echo the_title();?></h2>
                                <p  id="slidercontentformserver"><?php echo the_content();?></p>
                                <div class"offer-slider-btn"><a href="<?php echo the_permalink();?>"><span>Read More</span></a></div>
                            </div>
                        </div>
                    </div>
                    </div>
                     <?php endwhile; ?>
                    </div>
                </div>
            </div>     

		<!-- popular destinations -->
        <div class="mp-pop">
		<div class="wrapper-padding-a">
			<div class="mp-popular popular-destinations">
				<header class="fly-in">
					<b>Popular Destinations</b>
					<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
				</header>
				
				<?php 
				$post_id = "";
                query_posts('cat=7'); ?>
				<div class="fly-in mp-popular-row" >
					<!-- // -->
					<?php while(have_posts()) : the_post();?>
						<div class="offer-slider-i">
							<a class="offer-slider-img" href="#">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>" alt=""/>
								<span class="offer-slider-overlay">
								</span>
								<span class="offer-slider-btn">view details</span>								
							</a>
							<div class="offer-slider-txt">
								<div class="offer-slider-link"><a href="#">Paris, france</a></div>
								<div class="offer-slider-l">
									<div class="offer-slider-location">11 NOV 2014 - 22 NOV 2014</div>
									<nav class="stars">
										<ul>
											<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
											<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
											<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
											<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
											<li><a href="#"><img alt="" src="img/star-a.png" /></a></li>
										</ul>
										<div class="clear"></div>
									</nav>
								</div>
								<div class="offer-slider-r align-right">
									<b>450$</b>
									<span>price</span>
								</div>
								<div class="offer-slider-devider"></div>								
								<div class="clear"></div>
							</div>
						</div>
						<?php endwhile;?>
				</div>
				<div class="clear"></div>						
			</div>

			<div class="mp-popular">
				<header class="fly-in">
					<b>Who are we?</b>
					<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
					Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
					Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
					Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
					Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
				</header>
				
			</div>

		</div>
	</div>


	

        <div class="other-services">
            <?php 	
            $post_id = "";
            query_posts('cat=6'); ?>
            <div id="service-slider" class="owl-carousel owl-theme">
                <?php while (have_posts()) : the_post(); ?>
                <div class="item">
                    <img class="slider-height" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>" alt="<?php echo the_title();?>">
                    <div class="offer-slider-txt">
                        <div class="offer-slider-link"><h><?php echo the_title();?></h></div>
                        <div class="offer-slider-location"><?php echo the_content();?></div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div> 

        <div class="mp-popular ">
    
		<header class="fly-in">
			<b>Our Recent Blog Updates</b>
			<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
		</header>
      	<div class="blog-content">
			<ul>
				<?php
					$args = array( 'numberposts' => '5' );
					$recent_posts = wp_get_recent_posts( $args );
					foreach( $recent_posts as $recent ){
						echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"]. '' .  get_the_post_thumbnail($recent['ID'], 'thumbnail'). '</a> </li> ';
					}
				?>
			</ul>
		</div>
    </div>
        <?php wp_reset_query(); ?>
                          
	
<?php
get_footer();
