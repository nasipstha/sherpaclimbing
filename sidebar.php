<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nepal
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<?php
if ( ! is_active_sidebar( 'sidebar-2' ) ) {
	return;
}
?>

<aside id="secondary " class="widget-area ">
	<div class="widget-area-1">
	<ul>
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</ul>
    </div>

	
    <div class="widget-area-2">
	<ul>
	<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</ul>
    </div>
	
</aside><!-- #secondary -->

<!-- new sidebar -->
