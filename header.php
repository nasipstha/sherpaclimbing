<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Treaking_Hub_Nepal
 */

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>GARO ESTATE | Home page</title>
        <meta name="description" content="GARO is a real-estate template">
        <meta name="author" content="Kimarotec">
        <meta name="keyword" content="html5, css, bootstrap, property, real-estate theme , bootstrap template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/fontello.css">
        <link href="<?php echo esc_url( get_template_directory_uri() );?>/assets/fonts/icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">
        <link href="<?php echo esc_url( get_template_directory_uri() );?>/assets/fonts/icon-7-stroke/css/helper.css" rel="stylesheet">
        <link href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/animate.css" rel="stylesheet" media="screen">
        <link rel="<?php echo esc_url( get_template_directory_uri() );?>/stylesheet" href="assets/css/bootstrap-select.min.css"> 
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/icheck.min_all.css">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/price-range.css">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/owl.carousel.css">  
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/owl.theme.css">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/owl.transitions.css">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/style.css">
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>  /style.css">        
        <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/responsive.css">
        <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/jquery-1.10.2.min.js"></script> 
        <link href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/animate.min.css" rel="stylesheet">
       <link href="<?php echo esc_url( get_template_directory_uri() );?>/assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/bootstrap.min.js"></script>

    <!-- Bootstrap Dropdown Hover JS -->
    <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/bootstrap-dropdownhover.min.js"></script>
    <style>
     
.dropdown-menu > li.kopie > a {
    padding-left:5px;
}
 
.dropdown-submenu {
    position:relative;
}
.dropdown-submenu>.dropdown-menu {
   top:0;left:100%;
   margin-top:-6px;margin-left:-1px;
   -webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;
 }
  
.dropdown-submenu > a:after {
  border-color: transparent transparent transparent #333;
  border-style: solid;
  border-width: 5px 0 5px 5px;
  content: " ";
  display: block;
  float: right;  
  height: 0;     
  margin-right: -10px;
  margin-top: 5px;
  width: 0;
}
 .navbar-nav{

     float:right;
 }
.dropdown-submenu:hover>a:after {
    border-left-color:#555;
 }

.dropdown-menu > li > a:hover, .dropdown-menu > .active > a:hover {
  text-decoration: underline;
}  
  
@media (max-width: 767px) {
  .navbar-nav  {
     display: inline;
  }
  .navbar-default .navbar-brand {
    display: inline;
  }
  .navbar-default .navbar-toggle .icon-bar {
    background-color: #fff;
  }
  .navbar-default .navbar-nav .dropdown-menu > li > a {
    color: red;
    background-color: #ccc;
    border-radius: 4px;
    margin-top: 2px;   
  }
   .navbar-default .navbar-nav .open .dropdown-menu > li > a {
     color: #333;
   }
   .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
   .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
     background-color: #ccc;
   }

   .navbar-nav .open .dropdown-menu {
     border-bottom: 1px solid white; 
     border-radius: 0;
   }
  .dropdown-menu {
      padding-left: 10px;
  }
  .dropdown-menu .dropdown-menu {
      padding-left: 20px;
   }
   .dropdown-menu .dropdown-menu .dropdown-menu {
      padding-left: 30px;
   }
   li.dropdown.open {
    border: 0px solid red;
   }

}
 
@media (min-width: 768px) {
  ul.nav li:hover > ul.dropdown-menu {
    display: block;
  }
  #navbar {
    text-align: center;
  }
}  

    </style>

    </head>
    <body>
    <span class="imagepath" data-path="<?php echo esc_url(get_template_directory_uri());?>"></span>

    <header id="top">
	<div class="header-a">
		<div class="wrapper-padding">
			<div class="header-phone"><span>1 - 555 - 555 - 555</span></div>
			<div class="header-social">
				<a href="#" class="social-twitter"></a>
				<a href="#" class="social-facebook"></a>
				<a href="#" class="social-vimeo"></a>
				<a href="#" class="social-pinterest"></a>
				<a href="#" class="social-instagram"></a>
			</div>
			
			
			<div class="clear"></div>
		</div>
	</div>
	<div class="header-b">
		<!-- // mobile menu // -->
    <div class="mobile-menu">
				<nav>
        <?php 
	      					$args =array(
                    'depth' => 2,
                'theme_location'=>'primary'	,						
									);
								wp_nav_menu($args); 
				 ?>
				</nav>	
			</div>
		<!-- \\ mobile menu \\ -->
			
		<div class="wrapper-padding">
			<div class="header-logo"><a href="#"><img src="<?php echo esc_url(get_template_directory_uri());?>/assets/images/logo.png" alt=" "></a></div>
			<div class="header-right">
		<?php echo get_search_form();?>
				<div class="hdr-srch-devider"></div>
				<a href="#" class="menu-btn"></a>
				<nav class="header-nav">
               <?php 
	      					$args =array(
                    'depth' => 2,
                'theme_location'=>'primary'	,
                'menu_id'=>'primary-menu'							
									);
								wp_nav_menu($args); 
				 ?>
				</nav>
				
			</div>
			<div class="clear"></div>
		</div>
</header>