<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Treaking_Hub_Nepal
 */

?>

<article>


<div class="page-head"> 
            <div class="container">
                <div class="row">
                    <div class="page-head-content">
                        <h1 class="page-title"><?php echo the_title(); ?> </h1>               
                    </div>
                </div>
            </div>
        </div>



</article>
