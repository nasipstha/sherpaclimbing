<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Treaking_Hub_Nepal
 */

?>

<!-- main-cont -->
<div class="main-cont">  	
	
	<div class="inner-page">
		<div class="inner-breadcrumbs">
			<div class="content-wrapper">
				<div class="page-title">Blog with sidebar</div>
				<div class="breadcrumbs">
        			<a href="#">Home</a> / <span>Blog with sidebar</span>
      			</div>
      			<div class="clear"></div>
      		</div>		
		</div>
		
	</div>
	
	<div class="blog-page">
		<div class="content-wrapper">



		<div class="blog-sidebar">
			<div class="blog-sidebar-l">
  				<div class="blog-sidebar-lb">
    				<div class="blog-sidebar-p">
						
						<div class="blog-row">
							<!-- // -->
								<div class="blog-post">
								<div class="blog-post-i">
									<div class="blog-post-l">
										<div class="blog-post-date">
											<b>05</b>
											<span>Mar, 2015</span>
										</div>
										<div class="blog-post-info">
											<div>by Admin</div>
											<div>posted in business</div>
											<div>5 comments</div>
										</div>
									</div>
									<div class="blog-post-c">
  										<div class="blog-post-cb">
    										<div class="blog-post-p">
												<div class="blog-post-title"><a href="#">Standard blog format</a></div>
    											<div class="blog-post-preview">
    												<div class="blog-post-img">
    													<a href="#"><img alt="" src="img/blogpost-01.jpg" /></a>
    												</div>
    											</div>
    											<div class="blog-post-txt">Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta. sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</div>
    											<a href="#" class="blog-post-more">more</a>
    										</div>
  										</div>
  										<div class="clear"></div>
									</div>
								</div>
								<div class="clear"></div>
								</div>
							<!-- \\ -->
							<!-- // -->
								<div class="blog-post">
								<div class="blog-post-i">
									<div class="blog-post-l">
										<div class="blog-post-date">
											<b>03</b>
											<span>Mar, 2015</span>
										</div>
										<div class="blog-post-info">
											<div>by Admin</div>
											<div>posted in business</div>
											<div>5 comments</div>
										</div>
									</div>
									<div class="blog-post-c">
  										<div class="blog-post-cb">
    										<div class="blog-post-p">
												<div class="blog-post-title"><a href="#">gallery blog format</a></div>
    											<div class="blog-post-preview">
    												<div class="blog-post-slider">
    													<div class="blog-post-st">
    														<div class="blog-post-slider-i"><a href="#"><img alt="" src="img/blogpost-02.jpg" /></a></div>
    														<div class="blog-post-slider-i"><a href="#"><img alt="" src="img/2-4.jpg" /></a></div>
    														<div class="blog-post-slider-i"><a href="#"><img alt="" src="img/3-1.jpg" /></a></div>
    													</div>
    												</div>
    											</div>
    											<div class="blog-post-txt">Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta. sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</div>
    											<a href="#" class="blog-post-more">more</a>
    										</div>
  										</div>
  										<div class="clear"></div>
									</div>
								</div>
								<div class="clear"></div>
								</div>
							<!-- \\ -->
							<!-- // -->
								<div class="blog-post">
								<div class="blog-post-i">
									<div class="blog-post-l">
										<div class="blog-post-date">
											<b>03</b>
											<span>Mar, 2015</span>
										</div>
										<div class="blog-post-info">
											<div>by Admin</div>
											<div>posted in business</div>
											<div>5 comments</div>
										</div>
									</div>
									<div class="blog-post-c">
  										<div class="blog-post-cb">
    										<div class="blog-post-p">
												<div class="blog-post-title"><a href="#">audio post format!</a></div>
    											<div class="blog-post-preview">
    												<div class="blog-post-audio">
    													<audio src="build/AirReview-Landmarks-02-ChasingCorporate.mp3" controls="controls"></audio>
    												</div>
    											</div>
    											<div class="blog-post-txt">Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta. sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</div>
    											<a href="#" class="blog-post-more">more</a>
    										</div>
  										</div>
  										<div class="clear"></div>
									</div>
								</div>
								<div class="clear"></div>
								</div>
							<!-- \\ -->
							<!-- // -->
								<div class="blog-post">
								<div class="blog-post-i">
									<div class="blog-post-l">
										<div class="blog-post-date">
											<b>01</b>
											<span>Mar, 2015</span>
										</div>
										<div class="blog-post-info">
											<div>by Admin</div>
											<div>posted in business</div>
											<div>5 comments</div>
										</div>
									</div>
									<div class="blog-post-c">
  										<div class="blog-post-cb">
    										<div class="blog-post-p">
												<div class="blog-post-title"><a href="#">Standard blog format</a></div>
    											<div class="blog-post-preview">
    												<div class="blog-post-img">
    													<a href="#"><img alt="" src="img/blogpost-03.jpg" /></a>
    												</div>
    											</div>
    											<div class="blog-post-txt">Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta. sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</div>
    											<a href="#" class="blog-post-more">more</a>
    										</div>
  										</div>
  										<div class="clear"></div>
									</div>
								</div>
								<div class="clear"></div>
								</div>
							<!-- \\ -->
							<!-- // -->
								<div class="blog-post">
								<div class="blog-post-i">
									<div class="blog-post-l">
										<div class="blog-post-date">
											<b>01</b>
											<span>Mar, 2015</span>
										</div>
										<div class="blog-post-info">
											<div>by Admin</div>
											<div>posted in business</div>
											<div>5 comments</div>
										</div>
									</div>
									<div class="blog-post-c">
  										<div class="blog-post-cb">
    										<div class="blog-post-p">
    											<div class="blog-post-preview">
    												<div class="blog-post-qoute">
    													<div class="blog-post-qoute-a">Accusantium doloremque laudantium totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto</div>
    													<div class="blog-post-qoute-b">Sam Rojers</div> 														
    												</div>
    											</div>
    											<div class="blog-post-txt">Doloremque laudantium, totam rem. Aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta. sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</div>
    											<a href="#" class="blog-post-more">more</a>
    										</div>
  										</div>
  										<div class="clear"></div>
									</div>
								</div>
								<div class="clear"></div>
								</div>
							<!-- \\ -->		
						</div>				
						<nav class="blog-pagination">
							<ul>
								<li><a href="#"><img alt="" src="img/pag-left.png" /></a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#"><img alt="" src="img/pag-right.png" /></a></li>
							</ul>
							<div class="clear"></div>
						</nav>
						
    				</div>
  				</div>
  				<br class="clear" />
			</div>
		</div>
		<div class="blog-sidebar-r">
  			<!-- // widget // -->
  				<div class="blog-widget search-widget">
  					<h2>Search</h2>
  					<input type="text" value="" placeholder="To search type and hit enter">
  				</div>
  			<!-- \\ widget \\ -->
  			<!-- // widget // -->
  				<div class="blog-widget text-widget">
  					<h2>text widget</h2>
  					<p>Perspiciatis unde omnis iste natus doxes sit voluptatem accusantium doloremque la dantiumeaque ipsa.</p>
  				</div>
  			<!-- \\ widget \\ -->
  			<!-- // widget // -->
  				<div class="blog-widget recent-widget">
  					<h2>RECENT POSTS</h2>
  					<nav>
  						<ul>
  							<li><a href="#">Art</a></li>
  							<li><a href="#">Business</a></li>
  							<li><a href="#">Design</a></li>
  							<li><a href="#">Travel</a></li>	
  						</ul>
  					</nav>
  				</div>
  			<!-- \\ widget \\ -->
  			<!-- // widget // -->
  				<div class="blog-widget tweeter-widget">
  					<h2>recent tweets</h2>
  					<div class="tweets-row">
  						<!-- // -->
  						<div class="tweeter-item">
  							<div class="tweeter-item-l"></div>
  							<div class="tweeter-item-r">
  								<span>Unde omnis iste natus doxes sit voluptatem accusantium doloremque</span>
  								<b>About 20 days ago</b>
  							</div>
  							<div class="clear"></div>
  						</div>
  						<!-- \\ -->
  						<!-- // -->
  						<div class="tweeter-item">
  							<div class="tweeter-item-l"></div>
  							<div class="tweeter-item-r">
  								<span>Iste natus doxes sit voluptatem accusantium dolorem</span>
  								<b>About 20 days ago</b>
  							</div>
  							<div class="clear"></div>
  						</div>
  						<!-- \\ -->
  						<!-- // -->
  						<div class="tweeter-item">
  							<div class="tweeter-item-l"></div>
  							<div class="tweeter-item-r">
  								<span>Voluptatem accusantium dolorem la dantium eaque ipsa.</span>
  								<b>About 20 days ago</b>
  							</div>
  							<div class="clear"></div>
  						</div>
  						<!-- \\ -->
  					</div>
  				</div>
  			<!-- \\ widget \\ -->
  			<!-- // widget // -->
  				<div class="blog-widget tags-widget">
  					<h2>Tags</h2>
  					<div class="tags-row">
  						<a href="#">PEoples</a>
  						<a href="#">Design</a>
  						<a href="#">Technology</a>
  						<a href="#">Music</a>
  						<a href="#">photography</a>
  					</div>
  					<div class="clear"></div>
  				</div>
  			<!-- \\ widget \\ -->
		</div>
		<div class="clear"></div>

		</div>
	</div>
	
</div>
<!-- /main-cont -->