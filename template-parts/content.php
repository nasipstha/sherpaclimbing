<!-- main-cont -->
<div class="main-cont">
  <div class="body-wrapper">
    <div class="wrapper-padding">
    <div class="page-head">
      <div class="page-title"><?php echo the_title();?></div>
      <div class="breadcrumbs">
        <a href="#">Home</a> / <a href="#">Hotel</a> / <span>List style one</span>
      </div>
      <div class="clear"></div>
    </div>

	<div class="sp-page">

		<div class="sp-page-a">
			<div class="sp-page-l">
  				<div class="sp-page-lb">
  				<div class="sp-page-p">
  					<div class="mm-tabs-wrapper">
  					<!-- // tab item // -->
  						<div class="tab-item">
  							<div class="flight-image">
  								<span>Lufthansa passengers quickly, ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab.</span>
  								<img class="img-responsive" alt="<?php echo the_title();?>" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id()); ?>">
  							</div>
  						</div>
  					<!-- \\ tab item \\ -->	
  				
  									
  				</div>
  				
  				<div class="content-tabs">
  					<div class="content-tabs-head">
  						<nav>
  							<ul>
  								<li><a class="active" href="#"><?php echo 'Description';?></a></li>
  								<li><a href="#">features</a></li>
  								<li><a href="#">Price</a></li>									
  								<li><a href="#">Company Rules</a></li>

                			</ul>
  						</nav>
							<br />
						
  						<div class="clear"></div>
  					</div>
  					<div class="content-tabs-body">
  						<!-- // content-tabs-i // -->
  						<div class="content-tabs-i">
  						
  						
  							<div class="flight-d-text">
  								<!-- <h2>About Lufthansa</h2> -->
  								<p><?php echo the_field('Description');?></p>
  								<!-- <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p> -->
  							</div>
  							
  						</div>
  						<!-- \\ content-tabs-i \\ -->
  						<!-- // content-tabs-i // -->
  						<div class="content-tabs-i">
  							
							<div class="flight-d-text">
  								<!-- <h2>About Lufthansa</h2> -->
  								<p><?php echo the_field('Itenary');?></p>
  								<!-- <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p> -->
  							</div>
  						</div>
  						<!-- \\ content-tabs-i \\ -->
							<div class="content-tabs-i">
  							
							<div class="flight-d-text">
  								<!-- <h2>About Lufthansa</h2> -->
  								<p><?php echo the_field('price');?></p>
  								<!-- <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p> -->
  							</div>
  						</div>
  						<!-- \\ content-tabs-i \\ -->

              <!-- // content-tabs-i // -->
  						<div class="content-tabs-i">
                <h2>FAQ</h2>
                <p class="small">Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui voluptatem sequi nesciunt. </p>
                <div class="todo-devider"></div>
                <div class="faq-row">
                  <!-- // -->
                    <div class="faq-item">
                      <div class="faq-item-a">
                        <span class="faq-item-left">Totam rem aperiam, eaquie ipsa quae?</span>
                        <span class="faq-item-i"></span>
                        <div class="clear"></div>
                      </div>
                      <div class="faq-item-b">
                        <div class="faq-item-p">
                          Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia aspernatur aut odit aut fugit consequuntur magni dolores eos qui voluptatem sequi nesciunt. aspernatur aut odit aut fugit  
                        </div>
                      </div>
                    </div>
                  <!-- \\ -->
                  <!-- // -->
                    <div class="faq-item">
                      <div class="faq-item-a">
                        <span class="faq-item-left">Dolores eos qui ratione voluptatem sequi nescuin?</span>
                        <span class="faq-item-i"></span>
                        <div class="clear"></div>
                      </div>
                      <div class="faq-item-b">
                        <div class="faq-item-p">
                          Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia aspernatur aut odit aut fugit consequuntur magni dolores eos qui voluptatem sequi nesciunt. aspernatur aut odit aut fugit  
                        </div>
                      </div>
                    </div>
                  <!-- \\ -->
                  <!-- // -->
                    <div class="faq-item">
                      <div class="faq-item-a">
                        <span class="faq-item-left">Neque porro quisquam est, qui dolorem ipsum?</span>
                        <span class="faq-item-i"></span>
                        <div class="clear"></div>
                      </div>
                      <div class="faq-item-b">
                        <div class="faq-item-p">
                          Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia aspernatur aut odit aut fugit consequuntur magni dolores eos qui voluptatem sequi nesciunt. aspernatur aut odit aut fugit  
                        </div>
                      </div>
                    </div>
                  <!-- \\ -->
                  <!-- // -->
                    <div class="faq-item">
                      <div class="faq-item-a">
                        <span class="faq-item-left">Dolor sit amet consectutur adipisci velit, sed?</span>
                        <span class="faq-item-i"></span>
                        <div class="clear"></div>
                      </div>
                      <div class="faq-item-b">
                        <div class="faq-item-p">
                          Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia aspernatur aut odit aut fugit consequuntur magni dolores eos qui voluptatem sequi nesciunt. aspernatur aut odit aut fugit  
                        </div>
                      </div>
                    </div>
                  <!-- \\ -->
                  <!-- // -->
                    <div class="faq-item">
                      <div class="faq-item-a">
                        <span class="faq-item-left">Consectetur, adipisci velit, sed quia non numquam?</span>
                        <span class="faq-item-i"></span>
                        <div class="clear"></div>
                      </div>
                      <div class="faq-item-b">
                        <div class="faq-item-p">
                          Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia aspernatur aut odit aut fugit consequuntur magni dolores eos qui voluptatem sequi nesciunt. aspernatur aut odit aut fugit  
                        </div>
                      </div>
                    </div>
                  <!-- \\ -->
                </div>
              </div>
              <!-- \\ content-tabs-i \\ -->
              
  					</div>
  				</div>

  				</div>
  				
  				</div>
  				<div class="clear"></div>
			</div>
		</div>


		<div class="sp-page-r">
			<div class="h-detail-r">
				<div class="h-detail-lbl">
					<div class="h-detail-lbl-a">Vienna to New York</div>
					<div class="h-detail-lbl-b">ONEWAY FLIGHT</div>
				</div>
				<div class="h-detail-stars fligts-s">
					<div class="flight-line-a">
                        <b>departure</b>
                    	<span>14:12</span>
                	</div>
                	<div class="flight-line-d"></div>
                    <div class="flight-line-a">
                        <b>departure</b>
                    	<span>14:12</span>
                	</div>
                	<div class="flight-line-d"></div>
                    <div class="flight-line-a">
                        <b>departure</b>
                    	<span>14:12</span>
                	</div>
                	<div class="clear"></div>          
				</div>
				<div class="h-details-logo"><img alt="" src="img/h-logo.png"></div>
				<div class="h-details-text">
					<p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui voluptatem sequi nesciunt. </p>
				</div>
				<a href="#" class="book-btn">
					<span class="book-btn-l"><i></i></span>
					<span class="book-btn-r">BOOK FLIGHT NOW</span>
					<div class="clear"></div>
				</a>
			</div>
			
			<div class="h-help">
				<div class="h-help-lbl">Need Sparrow Help?</div>
				<div class="h-help-lbl-a">We would be happy to help you!</div>
				<div class="h-help-phone">2-800-256-124 23</div>
				<div class="h-help-email">sparrow@mail.com</div>
			</div>
			
			<div class="reasons-rating">
				<div id="reasons-slider">
          <!-- // -->
  				<div class="reasons-rating-i">
  					<div class="reasons-rating-txt">Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam.</div>
  					<div class="reasons-rating-user">
  						<div class="reasons-rating-user-l">
  							<img alt="" src="img/r-user.png">
  							<span>5.0</span>
  						</div>
  						<div class="reasons-rating-user-r">
  							<b>Gabriela King</b>
  							<span>from United Kingdom</span>
  						</div>
  						<div class="clear"></div>
  					</div>
  				</div>
  				<!-- \\ -->
          <!-- // -->
  				<div class="reasons-rating-i">
  					<div class="reasons-rating-txt">Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam.</div>
  					<div class="reasons-rating-user">
  						<div class="reasons-rating-user-l">
  							<img alt="" src="img/r-user.png">
  							<span>5.0</span>
  						</div>
  						<div class="reasons-rating-user-r">
  							<b>Robert Dowson</b>
  							<span>from Austria</span>
  						</div>
  						<div class="clear"></div>
  					</div>
  				</div>
  				<!-- \\ -->
          <!-- // -->
  				<div class="reasons-rating-i">
  					<div class="reasons-rating-txt">Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam.</div>
  					<div class="reasons-rating-user">
  						<div class="reasons-rating-user-l">
  							<img alt="" src="img/r-user.png">
  							<span>5.0</span>
  						</div>
  						<div class="reasons-rating-user-r">
  							<b>Mike Tyson</b>
  							<span>from France</span>
  						</div>
  						<div class="clear"></div>
  					</div>
  				</div>
  				<!-- \\ -->
        </div>
			</div>
			
			<div class="h-liked">
				<div class="h-liked-lbl">You May Also Like</div>
				<div class="h-liked-row">
					<!-- // -->
					<div class="h-liked-item">
					<div class="h-liked-item-i">
						<div class="h-liked-item-l">
  							<a href="#"><img alt="" src="img/like-01.jpg"></a>
						</div>
					<div class="h-liked-item-c">
  						<div class="h-liked-item-cb">
    						<div class="h-liked-item-p">
								<div class="h-liked-title"><a href="#">Andrassy Thai Hotel</a></div>
								<div class="h-liked-rating">
								 <nav class="stars">
									<ul>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-a.png" /></a></li>
									</ul>
									<div class="clear"></div>
								 </nav>
								</div>
								<div class="h-liked-foot">
									<span class="h-liked-price">850$</span>
									<span class="h-liked-comment">avg/night</span>
								</div>
    						</div>
  						</div>
  					<div class="clear"></div>
					</div>
					</div>
					<div class="clear"></div>	
					</div>
					<!-- \\ -->
					<!-- // -->
					<div class="h-liked-item">
					<div class="h-liked-item-i">
						<div class="h-liked-item-l">
  							<a href="#"><img alt="" src="img/like-02.jpg"></a>
						</div>
					<div class="h-liked-item-c">
  						<div class="h-liked-item-cb">
    						<div class="h-liked-item-p">
								<div class="h-liked-title"><a href="#">Campanile Cracovie</a></div>
								<div class="h-liked-rating">
								 <nav class="stars">
									<ul>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-a.png" /></a></li>
									</ul>
									<div class="clear"></div>
								 </nav>
								</div>
								<div class="h-liked-foot">
									<span class="h-liked-price">964$</span>
									<span class="h-liked-comment">avg/night</span>
								</div>
    						</div>
  						</div>
  					<div class="clear"></div>
					</div>
					</div>
					<div class="clear"></div>	
					</div>
					<!-- \\ -->
					<!-- // -->
					<div class="h-liked-item">
					<div class="h-liked-item-i">
						<div class="h-liked-item-l">
  							<a href="#"><img alt="" src="img/like-03.jpg"></a>
						</div>
					<div class="h-liked-item-c">
  						<div class="h-liked-item-cb">
    						<div class="h-liked-item-p">
								<div class="h-liked-title"><a href="#">Ermin's Hotel</a></div>
								<div class="h-liked-rating">
								 <nav class="stars">
									<ul>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-b.png" /></a></li>
										<li><a href="#"><img alt="" src="img/star-a.png" /></a></li>
									</ul>
									<div class="clear"></div>
								 </nav>
								</div>
								<div class="h-liked-foot">
									<span class="h-liked-price">500$</span>
									<span class="h-liked-comment">avg/night</span>
								</div>
    						</div>
  						</div>
  					<div class="clear"></div>
					</div>
					</div>
					<div class="clear"></div>	
					</div>
					<!-- \\ -->
				</div>			
			</div>
			
			<div class="h-reasons">
				<div class="h-liked-lbl">Reasons to Book with us</div>
				<div class="h-reasons-row">
				<!-- // -->
					<div class="reasons-i">
					<div class="reasons-h">
						<div class="reasons-l">
							<img alt="" src="img/reasons-a.png">
						</div>
						<div class="reasons-r">
  						<div class="reasons-rb">
							<div class="reasons-p">
								<div class="reasons-i-lbl">Awesome design</div>
								<p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p>
							</div>
  						</div>
  						<br class="clear" />
						</div>
					</div>
					<div class="clear"></div>
					</div>
				<!-- \\ -->	
				<!-- // -->
					<div class="reasons-i">
					<div class="reasons-h">
						<div class="reasons-l">
							<img alt="" src="img/reasons-b.png">
						</div>
						<div class="reasons-r">
  						<div class="reasons-rb">
							<div class="reasons-p">
								<div class="reasons-i-lbl">carefylly handcrafted</div>
								<p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p>
							</div>
  						</div>
  						<br class="clear" />
						</div>
					</div>
					<div class="clear"></div>
					</div>
				<!-- \\ -->	
				<!-- // -->
					<div class="reasons-i">
					<div class="reasons-h">
						<div class="reasons-l">
							<img alt="" src="img/reasons-c.png">
						</div>
						<div class="reasons-r">
  						<div class="reasons-rb">
							<div class="reasons-p">
								<div class="reasons-i-lbl">sustomer support</div>
								<p>Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunt.</p>
							</div>
  						</div>
  						<br class="clear" />
						</div>
					</div>
					<div class="clear"></div>
					</div>
				<!-- \\ -->				
				</div>
			</div>
			
			
		</div>
		<div class="clear"></div>
	</div>

    </div>	
  </div>  
</div>
<!-- /main-cont -->

<footer class="footer-a">
	<div class="wrapper-padding">
		<div class="section">
			<div class="footer-lbl">Get In Touch</div>
			<div class="footer-adress">Address: 58911 Lepzig Hore,<br />85000 Vienna, Austria</div>
			<div class="footer-phones">Telephones: +1 777 55-32-21</div>
			<div class="footer-email">E-mail: contacts@miracle.com</div>
			<div class="footer-skype">Skype: angelotours</div>
		</div>
		<div class="section">
			<div class="footer-lbl">Featured deals</div>
			<div class="footer-tours">
			<!-- // -->
			<div class="footer-tour">
				<div class="footer-tour-l"><a href="#"><img alt="" src="img/f-tour-01.jpg" /></a></div>
				<div class="footer-tour-r">
					<div class="footer-tour-a">amsterdam tour</div>
					<div class="footer-tour-b">location: netherlands</div>
					<div class="footer-tour-c">800$</div>
				</div>
				<div class="clear"></div>
			</div>
			<!-- \\ -->
			<!-- // -->
			<div class="footer-tour">
				<div class="footer-tour-l"><a href="#"><img alt="" src="img/f-tour-02.jpg" /></a></div>
				<div class="footer-tour-r">
					<div class="footer-tour-a">Kiev tour</div>
					<div class="footer-tour-b">location: ukraine</div>
					<div class="footer-tour-c">550$</div>
				</div>
				<div class="clear"></div>
			</div>
			<!-- \\ -->
			<!-- // -->
			<div class="footer-tour">
				<div class="footer-tour-l"><a href="#"><img alt="" src="img/f-tour-03.jpg" /></a></div>
				<div class="footer-tour-r">
					<div class="footer-tour-a">vienna tour</div>
					<div class="footer-tour-b">location: austria</div>
					<div class="footer-tour-c">940$</div>
				</div>
				<div class="clear"></div>
			</div>
			<!-- \\ -->
			</div>
		</div>
		<div class="section">
			<div class="footer-lbl">Twitter widget</div>
			<div class="twitter-wiget">
				<div id="twitter-feed"></div>
			</div>
		</div>
		<div class="section">
			<div class="footer-lbl">newsletter sign up</div>
			<div class="footer-subscribe">
				<div class="footer-subscribe-a">
					<input type="text" placeholder="you email" value="" />
				</div>
			</div>
			<button class="footer-subscribe-btn">Sign up</button>
		</div>
	</div>
	<div class="clear"></div>
</footer>

<footer class="footer-b">
	<div class="wrapper-padding">
		<div class="footer-left">© Copyright 2015 by weblionmedia. All rights reserved.</div>
		<div class="footer-social">
			<a href="#" class="footer-twitter"></a>
			<a href="#" class="footer-facebook"></a>
			<a href="#" class="footer-vimeo"></a>
			<a href="#" class="footer-pinterest"></a>
			<a href="#" class="footer-instagram"></a>
		</div>
		<div class="clear"></div>
	</div>
</footer>

  