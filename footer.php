<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nepal
 */

?>

<!-- footer-begins -->
<footer class="footer-a">
	<div class="wrapper-padding">
	<div class="row">
		<div class="col-sm-4 footer-padding">
			<div class="footer-lbl">Get In Touch</div>
			<div class="footer-adress">Address: 58911 Lepzig Hore,<br />85000 Vienna, Austria</div>
			<div class="footer-phones">Telephones: +1 777 55-32-21</div>
			<div class="footer-email">E-mail: contacts@miracle.com</div>
			<div class="footer-skype">Skype: angelotours</div>
		</div>
		
		<div class="col-sm-4 footer-padding">
		
		<div class="footer-lbl">Footer Links</div>
		<div class="footer-link">
		<?php
				wp_nav_menu( array(
					'theme_location' => 'footer',
					'menu_id'        => 'footer-menu'
				) );?>	
				</div>
</div>

<div class="col-sm-4 footer-padding">
			<div class="footer-lbl">Any Query??</div>

			<div class="footer-subscribe">
			<div class="footer-subscribe-a">
				<input type="text" placeholder="Your Name" value="" required="true" style="color:white"/>
				</div>
				</div>

				<div class="footer-subscribe">
				<div class="footer-subscribe-a">
				<input type="text" placeholder="Your Email" value="" required="true" style="color:white"/>
				
				</div>
			</div>

			<div class="footer-subscribe"style="height:50px">
				<div class="footer-subscribe-a">
			
				<input type="text" placeholder="Your Message...!!" value="" required="true"style="color:white"/>
				
				</div>
			</div>
			<button type="submit" class="footer-subscribe-btn">Submit</button>
		</div>
	</div>
	</div>
	
</footer>

<footer class="footer-b">
	<div class="wrapper-padding">
		<div class="footer-left">© Copyright 2018 by Kalopul Freelancer
		<span class="sep"> | </span> All rights reserved
		<span class="sep"> | </span>
		<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'nepal' ) ); ?>"><?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'nepal' ), 'WordPress' );
			?></a>
		
			 <?php
			// 	/* translators: 1: Theme name, 2: Theme author. */
			// 	printf( esc_html__( 'Theme: %1$s by %2$s.', 'nepal' ), 'nepal', '<a href="http://underscores.me/">Underscores.me</a>' );
			 ?>
		</div>
		<div class="footer-social">
			<a href="#" class="footer-twitter"></a>
			<a href="#" class="footer-facebook"></a>
			<a href="#" class="footer-vimeo"></a>
			<a href="#" class="footer-pinterest"></a>
			<a href="#" class="footer-instagram"></a>
		</div>
		<div class="clear"></div>
	</div>
</footer><!-- footer-ends -->

  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/idangerous.swiper.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/slideInit.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/owl.carousel.min.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/bxSlider.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/jqeury.appear.js"></script>  
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/script.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/main.js"></script>  
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/custom.select.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/jquery.ui.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/navigation.js"></script>
  <script src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/skip-link-focus-fix.js"></script>
  
  
  <script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() );?>/assets/js/twitterfeed.js"></script>

</body>
</html>
